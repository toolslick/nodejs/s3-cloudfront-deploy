"use strict";

const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const md5File = require("md5-file");
const md5 = require("md5");
const eol = require("eol");
const mime = require("mime-types");

const textFiles = [".svg", ".js", ".css", ".html", ".map"];
const mimeTypes = {
    ".ico": "image/x-icon"
};

const removeFilePart = fullPath => path.parse(fullPath).dir;

let allItems = [];
let awsItemMap = new Map();
let changedKeys = [];

const listS3Bucket = (s3, domain, nextToken, done) => {
    s3.listObjectsV2({
        Bucket: domain,
        MaxKeys: 100,
        ContinuationToken: nextToken
    }, (err, data) => {
        if (err)
            throw err;

        allItems = allItems.concat(data.Contents);

        if (data.IsTruncated) {
            listS3Bucket(s3, domain, data.NextContinuationToken, done);
        }
        else
            done();
    });
};


const addChangedKey = key => {
    changedKeys.push(`/${encodeURI(key)}`);
};

const getContentType = (file) => {
    const parsedFile = path.parse(file);
    const contentType = mimeTypes[parsedFile.ext] || mime.lookup(parsedFile.ext) || "application/octet-stream";
    console.log(`File: ${file}, Content Type: ${contentType}`);

    return contentType;
};

const getFileContent = (file, done) => {
    const parsedFile = path.parse(file);

    if (textFiles.indexOf(parsedFile.ext) !== -1) {
        fs.readFile(file, "utf8", (err, fileContents) => {
            done(eol.lf(fileContents));
        });
    } else {
        fs.readFile(file, (err, data) => {
            done(data);
        });
    }
};

const getMd5 = (file, done) => {
    const parsedFile = path.parse(file);

    if (textFiles.indexOf(parsedFile.ext) !== -1) {
        fs.readFile(file, "utf8", (err, fileContents) => {
            const md5Hash = md5(eol.lf(fileContents));
            done(md5Hash);
        });
    } else {
        md5File(file).then(done);
    }
};

const getS3Path = (config, file) => {
    const parsedPath = path.parse(file);

    if (config.extensionStripFiles.indexOf(parsedPath.ext) !== -1) {
        file = path.join(path.dirname(file), parsedPath.name);
    }

    const relativePath = path.relative(config.distDir, file).replace(/\\/g, "/");
    return relativePath;
};

const getMaxAge = (contentType, file) => {
    const fileName = path.basename(file);
    switch (fileName) {
        case "robots.txt":
        case "sitemap.xml":
            return 0;

        default:
            break;
    }

    switch (contentType) {
        case "text/html":
            return 60 * 60 * 24;

        case "application/json":
            return 60 * 60 * 24 * 30;

        case "image/x-icon":
        case "image/jpeg":
        case "image/webp":
        case "image/gif":
        case "image/png":
        case "font/woff":
        case "font/woff2":
        case "font/otf":
        case "application/vnd.ms-fontobject":
        case "font/ttf":
        case "text/plain":
        case "text/css":
        case "text/x-scss":
        case "application/javascript":
        case "application/xml":
        case "image/svg+xml":
        case "audio/wave":
            return 60 * 60 * 24 * 365;

        default:
            if (fileName === "_redirects")
                return "text/plain";    // fuse _redirects file

            throw `Unknown content type: ${contentType} for file: ${file}`;
    }
};

const uploadFile = (s3, domain, file, s3Path, localMd5Hash, contentType, maxAge) => {
    getFileContent(file, fileContent => {
        s3.upload({
            Bucket: domain,
            ContentType: contentType,
            Key: s3Path,
            Body: fileContent,
            ETag: localMd5Hash,
            CacheControl: `max-age=${maxAge}`
        }, (err, data) => {
            if (err)
                throw err;

            console.info(`Uploaded: ${data.Location}`);
        });
    });
};

const processDir = (s3, config, dir, done) => {
    fs.readdir(dir, (err, children) => {
        if (err)
            throw err;

        var pending = children.length;
        if (!pending)
            return done();

        children.forEach(child => {
            child = path.resolve(dir, child);

            fs.stat(child, (err, stat) => {
                if (stat && stat.isDirectory()) {
                    processDir(s3, config, child, () => {
                        if (!--pending)
                            done();
                    });
                }
                else {
                    const s3Path = getS3Path(config, child);
                    const s3Md5Hash = awsItemMap.get(s3Path);

                    getMd5(child, (localMd5Hash) => {
                        const contentType = getContentType(child);
                        const maxAge = getMaxAge(contentType, child);

                        if (err || !s3Md5Hash) {
                            // new file
                            console.info(`Uploading new file: ${s3Path} [${localMd5Hash}]`);
                            uploadFile(s3, config.domain, child, s3Path, localMd5Hash, contentType, maxAge);
                        } else if (s3Md5Hash !== localMd5Hash) {
                            console.info(`Uploading changed file: ${s3Path} = ${s3Md5Hash} != ${localMd5Hash}`);
                            uploadFile(s3, config.domain, child, s3Path, localMd5Hash, contentType, maxAge);

                            awsItemMap.delete(s3Path);

                            if (!config.invalidateHtml && path.extname(child) === ".html") {
                                console.info(`HTML file: ${child} will not be invalidated`);
                            } else if(maxAge === 0) {
                                console.info(`File: ${child} will not be invalidated as it has max-age=0`);
                            }   
                            else {
                                addChangedKey(s3Path);

                                const defaultDirPath = `${removeFilePart(s3Path)}/`;
                                console.log(`Adding default HTML path: ${defaultDirPath}`);
                                addChangedKey(defaultDirPath);
                            }
                        } else {
                            console.debug(`Unchanged: ${s3Path}`);
                            awsItemMap.delete(s3Path);
                        }

                        if (!--pending)
                            done();
                    });
                }
            });
        });
    });
};


const invalidateCloudFront = (cf, domain, changedItems) => {
    cf.listDistributions((err, data) => {
        let found = false;

        data.DistributionList.Items.forEach((item) => {
            item.Aliases.Items.forEach((alias) => {
                if (alias == domain) {
                    found = true;

                    console.info(`Invalidating CloudFront distribution: ${item.Id}`);

                    cf.createInvalidation({
                        DistributionId: item.Id,
                        InvalidationBatch: {
                            CallerReference: `${new Date().toISOString()}`,
                            Paths: {
                                Quantity: 1,
                                Items: ["/*"]
                            }
                        }
                    }, (err, data) => {
                        if (err) {
                            console.error("CloudFront invalidation failed");

                            console.debug(JSON.stringify(err));
                            return - 2;
                        }
                        else {
                            console.info(`CloudFront invalidation created ${JSON.stringify(data)}`);
                        }
                    });
                }
            });
        });

        if (!found)
            throw `Domain ${domain} was not found in CloudFront`;
    });
};

exports.deploy = (config) => {
    console.info(config);
    console.info(`Listing S3 bucket: ${config.domain}`);

    if (config.awsProfile) {
        console.info(`Using AWS profile: ${config.awsProfile}`)

        var credentials = new AWS.SharedIniFileCredentials({ profile: config.awsProfile });
        AWS.config.credentials = credentials;
    }

    var s3 = new AWS.S3();
    var cf = new AWS.CloudFront();

    console.info(`Listing bucket: ${config.domain}`);

    listS3Bucket(s3, config.domain, null, () => {
        console.info(`Found: ${allItems.length} files in S3`);

        allItems.forEach(item => {
            const etag = item.ETag.replace(/"/g, "");
            console.debug(`key: ${item.Key}, etag: ${etag}`);

            awsItemMap.set(item.Key, etag);
        });

        processDir(s3, config, config.distDir, () => {
            const keysToDelete = [];
            for (let key of awsItemMap.keys()) {
                console.info(`Deleting file: ${key}`);

                if (config.invalidateDeletedFiles) {
                    addChangedKey(key);
                }

                keysToDelete.push(key);
            }

            console.info(`Total changes: ${changedKeys.length}`);

            changedKeys.forEach(key => {
                console.debug(key);
            });

            if (changedKeys.length > 0) {
                invalidateCloudFront(cf, config.domain, changedKeys);
            }

            const objects = keysToDelete.map(k => ({
                Key: k
            }));

            while (objects.length) {
                const batchItems = objects.splice(0, 1000);

                s3.deleteObjects({
                    Bucket: config.domain,
                    Delete: {
                        Objects: batchItems
                    }
                }, (err) => {
                    if (err)
                        console.error(err);

                    console.info(`Deleted: ${batchItems.length} items`);
                });
            }
        });
    });
};
