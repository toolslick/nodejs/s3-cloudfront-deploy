# s3-cloudfront-deploy
A utility to deploy static website in an S3 bucket and deploy it via a CloudFront distribution

## Installing
`npm install --global s3-cloudfront-deploy`

### Example Usage
`s3-cloudfront-deploy -h my.web.site -d ./dist`

### Arguments
* -V, --version       output the version number
* -h, --host <host>   The host of the static website to deploy
* -d, --dist [value]  The distribution directory where static website files are stored (default is ./dist/ in current working directory)