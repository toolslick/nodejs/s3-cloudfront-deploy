#!/usr/bin/env node

const program = require("commander");
const logger = require("mx-color-logger");
const pkg = require("../package.json");
const deployer = require("../lib/deployer");
const fs = require("fs");

logger.init();

var version = pkg.version;

program
    .version(version)
    .option("-h, --host <host>", "The host of the static website to deploy")
    .option("-d, --dist [value]", "The distribution directory where static website files are stored (default is ./dist/ in current working directory)")
    .option("-p, --profile [value]", "The AWS profile to use for authentication")
    .option("-s, --strip-html-extension", "If specified extension of HTML files will be stripped while uploading")
    .option("--invalidate-deleted-files", "If specified deleted files are also invalidated")
    .option("--invalidate-html", "If specified invalidates HTML files")
    .parse(process.argv);


console.info("S3 CloudFront Deployer v " + version);

const domain = program.host;
const distDir = program.dist ? program.dist : "./dist";
const awsProfile = program.profile ? program.profile : null;
const stripHtmlExtension = program.stripHtmlExtension;
const invalidateDeletedFiles = program.invalidateDeletedFiles;
const invalidateHtml = program.invalidateHtml;

if (!domain) {
    console.error("Host name was not specified");
    program.outputHelp();
    process.exit(-1);
}

if (!fs.existsSync(distDir)) {
    console.error("The distribution directory does not exist");
    process.exit(-1);
}

if (!fs.statSync(distDir).isDirectory()) {
    console.error("The distribution directory is not a directory");
    process.exit(-1);
}

console.info(`Strip HTML Extension: ${stripHtmlExtension}`);

const extensionStripFiles = stripHtmlExtension ? [".html"] : [];

deployer.deploy(
    {
        extensionStripFiles,
        domain,
        distDir,
        awsProfile,
        invalidateDeletedFiles,
        invalidateHtml
    }
);